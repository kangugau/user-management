// import firebase from "firebase/app";
import "firebase/database";
import axios from "axios";

// var config = {
//   apiKey: "AIzaSyDKolL3tZnJtdcQojBGRnYWyrmaAoTt-Qc",
//   authDomain: "avt-training.firebaseapp.com",
//   databaseURL: "https://avt-training.firebaseio.com",
//   projectId: "avt-training",
//   storageBucket: "avt-training.appspot.com",
//   messagingSenderId: "728377640324"
// };
// firebase.initializeApp(config);
// export const database = firebase.database();

let api = {
  instance: axios.create({
    baseURL: "https://localhost:5001/api",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.token
    }
  }),
  addUser(user) {
    return this.instance.post("user", user);
  },
  updateUser(user) {
    let key = user.id;
    return this.instance.put(`user/${key}`, user);
  },

  getToken(user) {
    return this.instance.post("/auth", user);
  },
  getUsers() {
    return this.instance.get("user");
  },

  deleteUser(key) {
    return this.instance.delete(`user/${key}`);
  }
};

export default api;
