import Vue from "vue";
import Vuex from "vuex";
import api from "./api";
import router from "./router"
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: [],
    keyArr: [""],
    dialogVisible: false,
    token: localStorage.token
  },
  getters: {
    auth(state) {
      if (state.token) {
        return true;
      }
      return false;
    },
    loggedIn(state) {
      return state.auth;
    },
    canSignOut(state) {
      return state.auth;
    },
    displayedUsers(state) {
      return state.users.filter(user => {
        let count = 0;
        for (let key in state.keyArr) {
          for (let prop in user) {
            if (prop == "key") continue;
            if (
              user[prop] &&
              user[prop].toString().includes(state.keyArr[key])
            ) {
              count++;
              break;
            }
          }
        }
        if (count == state.keyArr.length) return true;
        else return false;
      });
    }
  },
  mutations: {
    setUsers(state, users) {
      state.users = users;
    },
    setKeyArr(state, keyArr) {
      state.keyArr = keyArr;
    },
    setToken(state, token) {
      state.token = token;
    },
    displayDialog(state) {
      state.dialogVisible = true;
    },
    closeDialog(state) {
      state.dialogVisible = false;
    }
  },
  actions: {
    setUsers(context) {
      api
        .getUsers()
        .then(res => {
          context.commit("setUsers", res.data);
        })
        .catch(err => {
          if (err.response.status == "401") context.dispatch("signOut");
        });
    },
    addUser(context, user) {
      api
        .addUser(user)
        .then(() => context.dispatch("setUsers"))
        .catch(err => {
          if (err.response.status == "401") context.dispatch("signOut");
        });
    },
    deleteUser(context, key) {
      api
        .deleteUser(key)
        .then(() => context.dispatch("setUsers"))
        .catch(err => {
          if (err.response.status == "401") context.dispatch("signOut");
        });
    },
    updateUser(context, user) {
      api
        .updateUser(user)
        .then(() => context.dispatch("setUsers"))
        .catch(err => {
          if (err.response.status == "401") context.dispatch("signOut");
        });
    },
    logIn(context, user) {
      return new Promise((resolve, reject) => {
        api
          .getToken(user)
          .then(res => {
            if (res.data) {
              localStorage.token = res.data;
              context.commit("setToken", res.data);
              api.instance.defaults.headers['Authorization'] = "Bearer " + res.data;
              resolve();
            } else {
              reject();
            }
          })
          .catch(err => {
            console.log(err);
          });
      });
    },
    signOut(context) {
      context.commit("setUsers", []);
      context.commit("setToken", null);
      delete localStorage.token;
      router.push("/login");
    },
    search(context, keyArr) {
      context.state.displayedUsers = context.state.users.filter(user => {
        for (let prop in user) {
          if (prop.value === keyArr) return true;
        }
        return false;
      });
    }
  }
});
